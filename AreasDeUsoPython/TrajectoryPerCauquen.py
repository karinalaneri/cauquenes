import folium
from geopy import distance
import webbrowser
import pandas as pd
import numpy as np
import glob
import pdb
import matplotlib
import matplotlib.pyplot as plt 
from matplotlib import cm
import branca.colormap as bcm
import folium.plugins
from geopy import distance 
from datetime import datetime, date, time, timedelta
import calendar
import math
from folium import plugins


df = pd.read_csv("Cauquenes_hojaGisClima.csv", sep=",")
cauqNames = []
cauqNames.append(np.unique(df["NOMBRE"]))
cauqNames = np.unique(np.concatenate(cauqNames).ravel())

dates=[]
dates.append(np.unique(df["FECHA"]))
dates=np.unique(np.concatenate(dates).ravel())


colors = ["red", "green", "yellow", "pink", "purple", "black"]


#coords de Puerto Madryn
coords = [-42.76133960629152, -65.03627305468594]
map1 = folium.Map(location=coords, zoom_start=5) 
# satelite map
folium.TileLayer("https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
                     attr="Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community").add_to(map1)
name = "trajectories.html"
minDat=1
jcol=0
for idcauq in cauqNames:
    if len(df.loc[df['NOMBRE'] == idcauq]) > minDat:
        points = zip(df.loc[df['NOMBRE'] == idcauq]["LAT"],df.loc[df['NOMBRE'] == idcauq]["LON"])
        points = list(points)
        folium.PolyLine(points, color=colors[jcol], weight=2.5, opacity=0.5).add_to(map1)
        for x in points:
            folium.CircleMarker(x, radius=3, stroke=False, fill=True,
                                    fill_color=colors[jcol], fill_opacity=1).add_to(map1)
    jcol=jcol+1
map1.save(name)


def plot_cauquen(df,idcauq):
    trajname="trajectory"+idcauq+".html"
    #coords de Puerto Madryn
    coords = [-42.76133960629152, -65.03627305468594]
    map2 = folium.Map(location=coords, zoom_start=5) 
# satelite map
    folium.TileLayer("https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
                     attr="Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community").add_to(map1)

    points = zip(df.loc[df['NOMBRE'] == idcauq]["LAT"],df.loc[df['NOMBRE'] == idcauq]["LON"])
    points = list(points)
    folium.PolyLine(points, color="red", weight=2.5, opacity=0.5).add_to(map2)
    for x in points:
        folium.CircleMarker(x, radius=3, stroke=False, fill=True,
                                    fill_color="red", fill_opacity=1).add_to(map2)
    
    map2.save(trajname)


def get_map():
    coords = [-42.76133960629152, -65.03627305468594]
    map1 = folium.Map(location=coords, zoom_start=5) 
    # satelite map
    folium.TileLayer("https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
                     attr="Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community").add_to(map1)
    return map1 

def rotated_grid(latt1, lont1, latt2,lont2,latcentro,loncentro):
    """time t1 previous to time t2 in the trajectory, the grid should be a "small" part of the surface"""
    """this function makes an NxN grid that covers the area between measured points lat,long"""
    N=12
    longrid=np.zeros(N*N)
    latgrid=np.zeros(N*N)
    modlatlon=math.sqrt((latt2-latt1)**2+(lont2-lont1)**2)
    n=0
    do=0.12
    for i in range(N):
        for j in range(N):
            latgrid[n] = latcentro + do*(i-N*0.5)*(latt2-latt1)/modlatlon + do*(j-N*0.5)*(lont1-lont2)/modlatlon
            longrid[n] = loncentro + do*(i-N*0.5)*(lont2-lont1)/modlatlon + do*(j-N*0.5)*(latt2-latt1)/modlatlon
            n=n+1

    #pdb.set_trace()
    return longrid, latgrid, N


def make_probability_map(longrid, latgrid,latt1,lont1,latt2,lont2,Lgrid):
    """This function makes a heat map with the cauquen probability of being in a given grid cell"""
    name="GrillaCauquen"
    map1=get_map()
    coordsprob=[]
    pointsgrid=[]
    measuredpoints=[]
    #print("distancia en metros ejemov",distance.distance([latt1,lont1],[latt2,lont2]).m)
    print("grilla de ",str(Lgrid)+"X"+str(Lgrid)," , Preparando mapa....")
    #test with rnd probability  
#    for i in range(len(longrid)-1):
            #random probability for each point of the grid
#            coordsprob.append([latgrid[i],longrid[i],np.random.rand()])
            #change for a function like Brownian Bridge
#            pointsgrid.append([latgrid[i],longrid[i]])
    
    coordsprob=gaussian_bridge(longrid, latgrid,latt1,lont1,latt2,lont2,Lgrid)
    
    measuredpoints=([latt1,lont1],[latt2,lont2])

    #to check grid orientation Ok
    #folium.PolyLine(pointsgrid, color="red", weight=5.5, opacity=0.9).add_to(map1)
    for po in measuredpoints:
        folium.CircleMarker(po, radius=5, stroke=False, fill=True,
                                    fill_color="pink", fill_opacity=1).add_to(map1)
    #to check grid is working
    #for x in pointsgrid:
    #    folium.CircleMarker(x, radius=3, stroke=False, fill=True,
    #                                fill_color="green", fill_opacity=1).add_to(map1)
    
    plugins.HeatMap(coordsprob).add_to(map1)
    

    map1.save(name)
    

def gaussian_bridge(longrid, latgrid,latt1,lont1,latt2,lont2,Lgrid):
    ### This function returns a probability of being at each point of the grid longrid latgrid 
    # (to use for the heat map done with make_probability_map function) 
    ### probability decays with distance from the measured points, and with distance from the 
    # line connecting the measured points (time is not considered now) 
    # Lgrid*Lgrid is the number of points in the grid
    pointorigin=(latt1,lont1)
    pointend=(latt2,lont2)
    pointcenter=((latt1+latt2)/2,(lont1+lont2)/2)
    DistBetweenPoints=int(distance.distance(pointorigin,pointend).m)
    CellSize=DistBetweenPoints/Lgrid
    coordandproba=[]
    
    for i in range(len(longrid)):
        pointgrid=(latgrid[i],longrid[i])
        distpointgridcentre=int(distance.distance(pointgrid,pointcenter).m)
        distpointextreme=min(int(distance.distance(pointgrid,pointorigin).m),int(distance.distance(pointgrid,pointend).m))
        distn=((DistBetweenPoints**2)/4-distpointextreme**2+distpointgridcentre)/DistBetweenPoints
        distt=math.sqrt(abs(distpointgridcentre**2-distn**2))    
        #proba inv prop a la distancia al punto observado
        #probapointgrid= distpointgridcentre/(DistBetweenPoints/2)
        probapointgrid=distn
        coordandproba.append([latgrid[i],longrid[i],probapointgrid])

    #sigmatmax=LGridCell*deltat/2
    #Nperp=int(sigmatmax/LGridCell)
    #Norm=math.sqrt(2*math.pi*sigmat**2)
    #proba_gauss=(math.exp(h**2/(2*sigmat**2)))/Norm
    #sigmat=t*(t-deltat)/Ngrid ###pensar esta dependencia
    
    return coordandproba


def read_times(df):
    ##read columns from file and generate a new column with correct format date/time
    days=pd.to_datetime(df["FECHA"],format="%d/%m/%Y")
    hour, minutes = divmod(df["HORA"], 1)
    hourint=hour.astype(int)
    minutesint= (minutes*100).astype(int)
    hourint = hourint.map("{:02}".format)
    minutesint = minutesint.map("{:02}".format)
    dftimes = pd.DataFrame({'year': days.dt.year,'month':days.dt.month,'day':days.dt.day,'hour': hourint,'minute': minutesint})
    completedate=pd.to_datetime(dftimes)
    df["COMPDATE"]=completedate     


########################################################################################
########################################################################################
## MAIN PROGRAM ###################################################
read_times(df)
#Tomo los datos de solo un cauquen
dfDIDO= df.loc[df['NOMBRE'] == "DIDO"]
plot_cauquen(df,"DIDO")

x1=dfDIDO.iloc[0:100]["LAT"].to_numpy()
x2=dfDIDO.iloc[0:100]["LON"].to_numpy()
#viedma 154 madryn 155
p1=(dfDIDO.iloc[154]["LAT"],dfDIDO.iloc[154]["LON"])
p2=(dfDIDO.iloc[155]["LAT"],dfDIDO.iloc[155]["LON"])
deltat=dfDIDO.iloc[155]["COMPDATE"]-dfDIDO.iloc[154]["COMPDATE"]
diff=dfDIDO["COMPDATE"].diff().dt.total_seconds()/60

deltaminutes=deltat.total_seconds()/60

latcentro=(p1[0]+p2[0])/2.
loncentro=(p1[1]+p2[1])/2.

longridcoords, latgridcoords, Ngrid=rotated_grid(p1[0], p1[1], p2[0],p2[1],latcentro,loncentro)

make_probability_map(longridcoords,latgridcoords,p1[0], p1[1], p2[0],p2[1], Ngrid)





# %%
# %%
