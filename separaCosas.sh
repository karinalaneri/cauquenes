#!/bin/bash
archivo=$1

mkdir Individuos 2> /dev/null #Crea la carpeta "Individuos"
IFS=$'\n'  # esta línea la pongo para que separe campos (que después voy a usar en el for) por newline, en lugar de espacios. Esto permite trabajar con archivos/carpetas que tengan espacios en el nombre.

# Este for guarda en la variable "$id" cada uno de los ids existentes, sin repetidos (sort|uniq)
for id in `cat $archivo | cut -d ";" -f5 | sort |uniq` ; do 
	echo $id 
	cat $archivo | grep ";$id;" > Individuos/id_${id}.csv  
done

